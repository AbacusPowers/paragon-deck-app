/**
 * Created by justin on 10/28/16.
 */
'use strict';

var express = require('express');
var paragon = require('paragon-api')(require('../../config.json'));

var router = express.Router();

router.get('/cards', function(req, res) {
    paragon.cardsGet(false, false, function(err, body){
        res.json({cards:body});
    })
});

module.exports = router;