/**
 * Created by justin on 10/28/16.
 */
'use strict';

angular.module('paragonDeckApp')
.service('dataService', function($http){
   this.getCards = function(cb) {
       $http.get('/api/cards').then(cb);
   };
});